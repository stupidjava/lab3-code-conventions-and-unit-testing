package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hw.db.models.Thread;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {

    private Thread thread;
    private threadController threadController;
    private final List<Post> posts = Collections.emptyList();

    @BeforeEach
    public void createTestThread() {
        thread = new Thread("author", new Timestamp(System.currentTimeMillis()), "forum1", "message", "slug", "title", 2);
        thread.setId(0);
        threadController = new threadController();
    }

    @Test
    public void testCheckIdOrSlug() {
        try(var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug("1"));
            assertNull(threadController.CheckIdOrSlug("2"));
        }
    }

    @Test
    public void testCreatePost() {
        try(var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), threadController.createPost("slug", posts));
        }
    }

    @Test
    public void testPosts() {
        try(var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getPosts(0, 100, 0, null, false
            )).thenReturn(posts);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts),
                    threadController.Posts("slug", 100, 0, null, false));
        }
    }

    @Test
    public void testChange() {
        try(var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) { Thread newThread = new Thread(
                    "author2",
                    new Timestamp(System.currentTimeMillis()),
                    "forum2",
                    "message",
                    "slug2",
                    "title",
                    90
            );
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(newThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), threadController.change("slug", newThread));

        }

    }

    @Test
    public void testInfo() {
        try (var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.info("slug"));
        }
    }

    @Test
    public void testVote() {
        try (var mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (var mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("nickname","user@mail.com", "fullname", "about");
                Vote vote = new Vote("nickname", 0);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info(vote.getNickname())).thenReturn(user);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                        threadController.createVote("slug", vote));
            }
        }
    }
}